// IMPORTS REQUIRED
var express      =   require("express"),
app              =   express(),
expressSanitizer =   require("express-sanitizer"),
methodOverride   =   require("method-override"),
bodyParser       =   require("body-parser"),
mongoose         =   require("mongoose"),
showdown         =   require("showdown");

// converter = new showdown.Converter(),
//     text      = "#hello, markdown!",
//     html      = converter.makeHtml(text);
//     console.log(html);
// APP CONFIG
mongoose.connect("mongodb://localhost/restblog");
app.set("view engine" ,"ejs");
app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended: true}));
app.use(expressSanitizer());
app.use(methodOverride("_method"));


// DATABASE CONFIG
var blogSchema;
blogSchema = new mongoose.Schema({
    title: String,
    body: String,
    image: String,
    created: {type: Date, default: Date.now}
});
var Blog = mongoose.model("Blog",blogSchema);


// INDEX ROUTES
app.get("/",function (req, res) {
    res.redirect("/blogs");
});
app.get("/blogs",function (req, res) {
    Blog.find({},function (err, blogs) {
       if(err){
           console.log(err);
       } else {
           res.render("index", {blogs: blogs});
       }
    });
});


//NEW ROUTE
app.get("/blogs/new", function (req, res) {
    res.render("new");
});



//CREATE ROUTE
app.post("/blogs", function (req, res) {
    var converter = new showdown.Converter(),
        mdtext = req.body.blog.body,
        html = converter.makeHtml(mdtext);
    req.body.blog.body = req.sanitize(html);
    Blog.create(req.body.blog,function (err, blog) {
        if(err){
            res.redirect("new");
        }else{
            res.redirect("blogs");
        }
    });
});



//SHOW ROUTE
app.get("/blogs/:id",function (req, res) {
    Blog.findById(req.params.id,function (err ,found) {
        if(err){
            res.redirect("/blogs");
        }else{
            res.render("show",{blog: found});
        }
    });

});



//EDIT ROUTE
app.get("/blogs/:id/editthisbloginageekyway", function (req, res) {
   Blog.findById(req.params.id, function (err, found) {
       if(err){
           res.redirect("/blogs");
       }else{
           res.render("edit",{blog: found});
       }
   });
});



//UPDATE ROUTE
app.put("/blogs/:id", function (req, res) {
    var converter = new showdown.Converter(),
        mdtext = req.body.blog.body,
        html = converter.makeHtml(mdtext);
    req.body.blog.body = req.sanitize(html);
    Blog.findByIdAndUpdate(req.params.id,req.body.blog,function (err,updatedBlog) {
       if(err){
           res.redirect("/blogs");
       } else{
           res.redirect("/blogs/" + req.params.id);
       }
    });
});



// DELETE ROUTE
app.delete("/blogs/:id", function (req, res) {
    Blog.findByIdAndRemove(req.params.id, function (err) {
        if(err){
            res.redirect("/blogs")
        }else {
            res.redirect("/blogs")
        }
    })
});



// SERVER LISTEN

app.listen(3001,function () {
    console.log("Blog Server has started, go to https://localhost:3001 to see the magic")
});